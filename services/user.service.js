const fs = require('fs');

const getUserById = (id) => {
  const content = fs.readFileSync("./userlist.json", "utf8");
  const users = JSON.parse(content);
  const user = users.find(users => users._id == id);
  return user;
};

const createUser = (body) => {
  const user = {
    _id: null,
    name: body.name, 
    health: body.health, 
    attack: body.attack, 
    defense: body.defense, 
    source: body.source
  };
   
  const dataUsers = fs.readFileSync("./userlist.json", "utf8");
  const users = JSON.parse(dataUsers);
   
  const id = Math.max.apply(Math, users.map (o => o._id));
  if(!Number.isFinite(id)){
    user._id = 1;
  } else {
    user._id = id + 1;
  }
  users.push(user);
  const data = JSON.stringify(users);
  // перезаписываем файл с новыми данными
  fs.writeFileSync("./userlist.json", data);
  return user;
};


const updateUser = (id, body) => {
  const dataUsers = fs.readFileSync("./userlist.json", "utf8");
  const users = JSON.parse(dataUsers);
  let user = users.find(users => users._id == id);

  if (user) {
    user.name = body.name;
    user.health = body.health;
    user.attack = body.attack;
    user.defense = body.defense;
    user.source = body.source;
     
    const data = JSON.stringify(users);
    fs.writeFile('./userlist.json', data, (err) => {
      if (err) throw err;
    });
    return user;
  } else {
    return null;
  }
};

const deleteUser = (id) => {
  const data = fs.readFileSync("./userlist.json", "utf8");
  const users = JSON.parse(data);
  let index = users.findIndex(u => id == u._id);

  if (index > -1) {
      const user = users.splice(index, 1)[0];
      const data = JSON.stringify(users);
      fs.writeFile("./userlist.json", data, (err) => {
        if (err) throw err;
      });
      return user;
  } else {
      return null;
  }
};


module.exports = {
  getUserById,
  createUser,
  updateUser,
  deleteUser
};
