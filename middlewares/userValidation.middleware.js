const { check, validationResult } = require('express-validator/check');

const userValidation = (req, res, next) => {
    req.checkBody("name", 'Name should be in range from 2 to 10').isLength({ min: 2, max: 20 });
    req.checkBody("health", 'Health should be number in range from 1 to 100').isInt({ min: 1, max: 100});
    req.checkBody("attack", 'Attack should be number in range from 1 to 10').isInt({ min: 1, max: 10});
    req.checkBody("defense", 'Defense should be number in range from 1 to 10').isInt({ min: 1, max: 10});
    req.checkBody("source", 'Source should be URL').isURL();
  
    const errors = req.validationErrors();
    if (!errors) {
      next();
    } else {
      res.status(422).jsonp(errors);
    }
  };

module.exports = {
    userValidation
}