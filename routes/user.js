const express = require('express');
const router = express.Router();
const fs = require('fs');

const { getUserById, createUser, updateUser, deleteUser } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");
const { userValidation } = require("../middlewares/userValidation.middleware");

//GET all players(users)
router.get('/', isAuthorized, (req, res, next) => {
  const content = fs.readFileSync("./userlist.json", "utf8");
  const users = JSON.parse(content);
  res.json(users);
});

//GET player(user) by id
router.get('/:id', isAuthorized, (req, res, next) => {
  const id = req.params.id;
  const user = getUserById(id);
    
  if (user) {
    res.json(user);
  } else {
    res.status(404).send(`Error: A user with id=${id} could not be found`);
    }
});

//POST - Created a user according to the data transmitted in the request body
router.post('/', isAuthorized, userValidation, (req, res, next) => {
  try {
    const newUser = createUser(req.body);
    res.json(newUser);
  } catch (err) {
    res.status(500).json(err);
  }
});

//PUT - Updated user by ID
router.put('/:id', isAuthorized, userValidation, (req, res, next) => {
  const id = req.params.id;
  const updatedUser = updateUser(id, req.body);

  if (updatedUser) {
    res.json(updatedUser);
  } else {
    res.status(404).send(`Error: A user with id=${id} could not be found`);
  }
});

//DELETE user by ID
router.delete('/:id', isAuthorized, (req, res, next) => {
  const id = req.params.id;
  const deletedUser = deleteUser(id);

  if (deletedUser) {
    res.status(204).send();
  } else {
    res.status(404).send(`Error: A user with id=${id} could not be found`);
  }
});

module.exports = router;