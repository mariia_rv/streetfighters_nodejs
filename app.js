const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const validator = require('express-validator');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/user');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser());
app.use(validator());

app.use('/', indexRouter);
app.use('/user', usersRouter);

module.exports = app;
