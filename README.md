# This is my first server, which I wrote with node.js and express.js

##Description
It server was created for get information about users from userlist.json. 

## Installation
npm i
npm start
By default server running on *localhost:3000*

##Dependencies
This project depends on Node.js. 
Other dependencies can be seen in the file **package.json**

## Support
If you have any difficulties or questions about using the package, create
[discussion] in this repository or email <mariya.ryabtsun@gmail.com>.

